﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Virecore.EntityFrameworkTools.Models
{
    /// <summary>
    /// Default property changed implementation.
    /// </summary>
    public class DefaultNotifyPropertyChanged : INotifyPropertyChanged
    {
        #region Public Members

        public event PropertyChangedEventHandler? PropertyChanged;

        #endregion

        #region Protected Methods
        
        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
