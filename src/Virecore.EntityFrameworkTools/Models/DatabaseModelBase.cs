﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Virecore.EntityFrameworkTools.Models
{
    /// <summary>
    /// Default Base class for database model.
    /// </summary>
    public class DatabaseModelBase<TId> : DefaultNotifyPropertyChanged
    {
        #region Public Members

#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        public TId Id { get; set; }     
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        public DateTime DateCreated { get; set; }
        public Guid CreatedById { get; set; }
        public DateTime DateModified { get; set; }
        public Guid ModifiedById { get; set; }
        public bool Deleted { get; set; }

        #endregion

        #region Public Methods

        /// <summary>
        /// Merge is used in EF DbContext.Update() method. Method merges all properties.
        /// This will override not set properties to null or default value!
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="updatedEntity"></param>
        public virtual void Merge<T>(T updatedEntity) where T : DatabaseModelBase<TId>
        {
            var properties = typeof(T).GetProperties();
            foreach (var property in properties)
            {
                var updatedValue = property.GetValue(updatedEntity);
                property.SetValue(this, updatedValue);
            }
        }
        #endregion
    }
}