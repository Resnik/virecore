﻿using System.Linq;
using System.Threading.Tasks;

namespace Virecore.EntityFrameworkTools.Repositories
{
    /// <summary>
    /// Generic interface for accessing and modifing data in database.
    /// </summary>
    /// <typeparam name="TBaseEntity">Base entity for EF</typeparam>
    /// <typeparam name="TId">Id Type (usually Guid or Int)</typeparam>
    public interface IEntityRepository<in TBaseEntity, in TId>
    {
        IQueryable<T> GetEntities<T>(bool includeDeleted = false, int pageNumber = -1, int pageSize = -1) where T : TBaseEntity;
        IQueryable<T> GetEntity<T>(TId entityId) where T : TBaseEntity;
        Task<bool> CreateEntity<T>(T entity) where T : TBaseEntity;
        Task<bool> UpdateEntity<T>(T entity) where T : TBaseEntity;
        Task<bool> DeleteEntity<T>(TId entityId, bool markForDeletion = false) where T : TBaseEntity;
        Task<bool> UndoDeletion<T>(TId entityId) where T : TBaseEntity;
    }
}
