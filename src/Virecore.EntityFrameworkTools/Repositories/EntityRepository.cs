﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Virecore.EntityFrameworkTools.Models;

namespace Virecore.EntityFrameworkTools.Repositories
{
    /// <summary>
    /// Default IEntityRepository implementation. 
    /// It uses included DatabaseModelBase class for base entity class and Guid for id Type.
    /// </summary>
    /// <typeparam name="TDbContext"></typeparam>
    /// <typeparam name="TId"></typeparam>
    public class EntityRepository<TDbContext, TId> : IEntityRepository<DatabaseModelBase<TId>, TId> where TDbContext : DbContext
    {
        #region Private Members

        private readonly TDbContext _context;
        private readonly ILogger<EntityRepository<TDbContext, TId>> _logger;

        #endregion

        #region Public Constructors
        
        public EntityRepository(TDbContext context, ILogger<EntityRepository<TDbContext, TId>> logger)
        {
            _logger = logger;
            _context = context;
        }

        #endregion

        #region Public Methods

        public async Task<bool> CreateEntity<T>(T entity) where T : DatabaseModelBase<TId>
        {
            _logger.LogInformation($"Creating entity {typeof(T)}");
            _context.Add(entity);
            var result = await _context.SaveChangesAsync();
            _logger.LogInformation($"Result -> affected: {result} rows, result: {result > 0}");
            return result > 0;
        }

        public async Task<bool> DeleteEntity<T>(TId entityId, bool markForDeletion = false) where T : DatabaseModelBase<TId>
        {
            _logger.LogInformation($"Deleting entity {typeof(T)}, Id: {entityId}");
            var entity = await GetEntity<T>(entityId).FirstOrDefaultAsync();
            if (entity == null)
                return false;

            if (markForDeletion)
                entity.Deleted = true;
            else
                _context.Remove(entity);

            var result = await _context.SaveChangesAsync();
            _logger.LogInformation($"Result -> affected: {result} rows, result: {result > 0}, marked for deletion: {markForDeletion}");
            return result > 0;
        }

        public IQueryable<T> GetEntities<T>(bool includeDeleted = false, int pageNumber = -1, int pageSize = -1) where T : DatabaseModelBase<TId>
        {
            IQueryable<T> query = _context.Set<T>(); //.AsNoTracking();

            query = query.Where(x => x.Deleted == false || x.Deleted == includeDeleted);

            if (pageNumber <= -1) 
                return query;

            pageNumber = pageNumber < 1 ? 1 : pageNumber;
            pageSize = pageSize < 1 ? 25 : pageSize; // default = 25 items per page
            var itemsToSkip = (pageNumber - 1) * pageSize;
            query = query.Skip(itemsToSkip).Take(pageSize);

            return query;
        }

        public IQueryable<T> GetEntity<T>(TId entityId) where T : DatabaseModelBase<TId>
        {
            IQueryable<T> query = _context.Set<T>(); //.AsNoTracking();
            query = query.Where(x => x.Id!.Equals(entityId));
            return query;
        }

        public async Task<bool> UndoDeletion<T>(TId entityId) where T : DatabaseModelBase<TId>
        {
            _logger.LogInformation($"Undoing deletion of entity {typeof(T)}, Id: {entityId}");
            var entity = await _context.FindAsync<T>(entityId);

            if (entity == null)
                return false;
            _context.Attach(entity);

            entity.Deleted = false;

            var result = await _context.SaveChangesAsync();
            _logger.LogInformation($"Result -> affected: {result} rows, result: {result > 0}");
            return result > 0;
        }

        public async Task<bool> UpdateEntity<T>(T entity) where T : DatabaseModelBase<TId>
        {
            _logger.LogInformation($"Updating entity {typeof(T)}, Id: {entity.Id}");
            /*var existingEntity = await GetEntity<T>(entity.Id).FirstOrDefaultAsync();
            if (existingEntity == null)
            {
                _logger.LogInformation($"Entity {typeof(T)}, Id: {entity.Id} does not exist!");
                return false;
            }

            _context.Attach(existingEntity);
            existingEntity.Merge(entity);*/

            _context.Attach(entity);

            var result = await _context.SaveChangesAsync();
            _logger.LogInformation($"Result -> affected: {result} rows, result: {result > 0}");
            return result > 0;
        }
        #endregion
    }
}
