﻿namespace Virecore.Environment
{
    /// <summary>
    /// Current system environment
    /// </summary>
    public class FrameworkEnvironment
    {
        #region Public Properties

        public bool IsDevelopment { get; } = true;

        public string Configuration => IsDevelopment ? "Development" : "Production";

        #endregion

        #region Constructor
        public FrameworkEnvironment()
        {
#if RELEASE
            IsDevelopment = false;
#endif
        }
        #endregion
    }
}
