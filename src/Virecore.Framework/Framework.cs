﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using Virecore.Environment;
using Virecore.Logging.File;
using Virecore.Profiling;
using Virecore.Profiling.ChromiumTracing;

namespace Virecore
{
    /// <summary>
    /// The main entry point for framework
    /// </summary>
    public static class Framework
    {
        #region Private Members

        /// <summary>
        /// DI Service Provider
        /// </summary>
        private static IServiceProvider? ServiceProvider { get; set; }

        #endregion
        
        #region Public Members

        /// <summary>
        /// Gets the configuration for environment
        /// </summary>
        public static IConfiguration Configuration => ServiceProvider.GetService<IConfiguration>();

        /// <summary>
        /// Gets the logger
        /// </summary>
        public static ILogger Logger => ServiceProvider.GetService<ILogger>();

        public static IProfiler Profiler => ServiceProvider.GetService<IProfiler>();

        /// <summary>
        /// Gets the environment
        /// </summary>
        public static FrameworkEnvironment Environment => ServiceProvider.GetService<FrameworkEnvironment>();

        #endregion

        #region Public Methods
        
        /// <summary>
        /// Call this function at the start of any application
        /// </summary>
        /// <param name="configure">Action to add custom configuration to the builder</param>
        /// <param name="injectedServices">Action to inject custom services in to service collection</param>
        public static void Startup(Action<IConfigurationBuilder>? configure = null, Action<IServiceCollection>? injectedServices = null)
        {
            // List of dependencies
            var services = new ServiceCollection();

            var environment = new FrameworkEnvironment();
            services.AddSingleton(environment);

            // Configuration
            var configurationBuilder = new ConfigurationBuilder()
                .AddEnvironmentVariables()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional:true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment.Configuration}.json", optional:true, reloadOnChange: true);

            // Custom configuration
            configure?.Invoke(configurationBuilder);

            // Inject configuration into services
            var configuration = configurationBuilder.Build();
            services.AddSingleton<IConfiguration>(configuration);

            // Logging as default
            services.AddLogging(options =>
            {
                options.AddConfiguration(configuration.GetSection("Virecore").GetSection("Logging"));
                options.AddConsole();
                options.AddDebug();
                options.AddLogFile(
                    configuration.GetSection("Virecore")
                    .GetSection("Logging")
                    .GetValue<string>("LogFilePath"));
            });

            services.AddDefaultLogger();

            services.AddProfiling(options =>
                {
                    options.AddProfilerFile(
                        configuration.GetSection("Virecore")
                            .GetSection("Profiling")
                            .GetValue<string>("ProfileFilePath"), configuration);
                });
            
            services.AddDefaultProfiler();

            // Custom service injection
            injectedServices?.Invoke(services);

            ServiceProvider = services.BuildServiceProvider();

            //Startup complete
            Logger.LogInformation($"Virecore started. Environment: [{environment.Configuration}]");
        }

        /// <summary>
        /// Shortcut to Framework.Provider.GetService to get an injected service of type <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T">The type of service to get</typeparam>
        /// <returns>Specified service from ServiceProvider</returns>
        public static T GetService<T>()
        {
            // Use provider to get the service
            return ServiceProvider.GetService<T>();
        }

        /// <summary>
        /// Shortcut to Framework.Provider.GetServices to get an injected services of type <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T">The type of service to get</typeparam>
        /// <returns>Specified service from ServiceProvider</returns>
        public static IEnumerable<T> GetServices<T>()
        {
            // Use provider to get the service
            return ServiceProvider.GetServices<T>();
        }
        #endregion
    }
}
