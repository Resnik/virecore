using Microsoft.Extensions.DependencyInjection;

namespace Virecore.Profiling
{
    public class ProfilerBuilder : IProfilerBuilder
    {
        public ProfilerBuilder(IServiceCollection services)
        {
            Services = services;
        }

        public IServiceCollection Services { get; }

    }
}