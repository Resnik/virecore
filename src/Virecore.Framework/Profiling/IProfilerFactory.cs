namespace Virecore.Profiling
{
    public interface IProfilerFactory
    {
        IProfiler CreateProfilerService(string profilerName);
    }
}