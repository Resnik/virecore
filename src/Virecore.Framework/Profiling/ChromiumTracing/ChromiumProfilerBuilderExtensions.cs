using Microsoft.Extensions.Configuration;

namespace Virecore.Profiling.ChromiumTracing
{
    public static class ChromiumProfilerBuilderExtensions
    {
        #region Public methods
        /// <summary>
        /// Add Profiler that writes to a file
        /// </summary>
        /// <param name="builder">Profile builder</param>
        /// <param name="path">Path to json file</param>
        /// <param name="configuration">Configuration</param>
        /// <returns></returns>
        public static  IProfilerBuilder AddProfilerFile(this IProfilerBuilder builder, string path, IConfiguration configuration)
        {
            builder.AddProfilerFactory(new ChromiumProfilerFactory(path, configuration));
            return builder;
        }
        #endregion
    }
}