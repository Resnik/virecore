using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using Microsoft.Extensions.Configuration;

namespace Virecore.Profiling.ChromiumTracing
{
    public class ChromiumProfiler : IProfiler
    {
        #region Static Properties
        /// <summary>
        /// List of file lock based on path
        /// </summary>
        private static readonly ConcurrentDictionary<string, object> Locks = new ConcurrentDictionary<string, object>();
        
        #endregion
        
        #region Private Members

        // path where profile will be created
        private string _filePath;
        private readonly IConfiguration _configuration;

        // file stream for created file
        private FileStream? CurrentFileStream { get; set; }

        // Session name of current profile
        private string? SessionName { get; set; }

        // Stopwatch
        private Stopwatch Stopwatch { get;} = new Stopwatch();

        #endregion

        #region Public members
         
        public bool IsRunning { get; set; }
        public bool IsEnabled 
        { 
            get 
            {
                if (_configuration == null)
                    return false;

                var boolValue = _configuration.GetSection("Virecore")?.GetSection("Profiling")?.GetValue<bool>("Enabled");

                return boolValue == null ? false : (bool)boolValue; 
            }
        }
            

        #endregion

        #region Private Methods

        /// <summary>
        /// Write text to file and optionally flush it
        /// </summary>
        /// <param name="text">string to write</param>
        /// <param name="flush">flush will push current stream changes to file</param>
        private void WriteToStream(string text, bool flush = true)
        {
            var fileLock = Locks.GetOrAdd(_filePath.ToUpper(), path => new object());
            
            var data = new UTF8Encoding(true).GetBytes(text);
            lock(fileLock)
            {
                CurrentFileStream?.Write(data, 0, data.Length);
                if (flush)
                    CurrentFileStream?.Flush();
            }
        }

        /// <summary>
        /// Writes header to json file
        /// </summary>
        private void WriteHeader()
        {
            WriteToStream("{\"otherData\": {},\"traceEvents\":[");
        }

        /// <summary>
        /// Writes footer to json file
        /// </summary>
        private void WriteFooter()
        {
            // move stream position back by 1 to remove last ',' from json profile array
            if (CurrentFileStream != null)
                CurrentFileStream.Position = CurrentFileStream.Position - 1;

            WriteToStream("]}");
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="filePath">Path to write to</param>
        /// <param name="configuration">Configuration to use</param>
        public ChromiumProfiler(string filePath, IConfiguration configuration)
        {
            filePath = Path.GetFullPath(filePath);
            _filePath = filePath;
            _configuration = configuration;
            
            var dir = Path.GetDirectoryName(_filePath);
            if(string.IsNullOrEmpty(dir))
                return;
                
            Directory.CreateDirectory(dir);
        }

        #endregion
        
        #region Public methods

        /// <summary>
        /// Starts the profiler
        /// </summary>
        /// <param name="sessionName">Name of the current session</param>
        public void StartProfiling(string? sessionName = null)
        {
            if (!IsEnabled)
                return;
            
            if (IsRunning)
                return;
            
            SessionName = sessionName;
            if (SessionName != null)
                _filePath = _filePath.Replace(".json",$"_{SessionName}.json");
            
            if (File.Exists(_filePath))
                File.Delete(_filePath);

            Stopwatch.Start();
            
            IsRunning = Stopwatch.IsRunning;
            if (!IsRunning) 
                return;
            
            CurrentFileStream = File.OpenWrite(_filePath);
            WriteHeader();
        }

        /// <summary>
        /// Stop the profiler
        /// </summary>
        public void StopProfiling()
        {
            if (!IsEnabled)
                return;
            
            if(!IsRunning)
                return;
            
            Stopwatch.Stop();
            
            WriteFooter();

            // Close and null the stream to clean up
            CurrentFileStream?.Close();
            CurrentFileStream = null;
            IsRunning = false;
        }

        /// <summary>
        /// Create a new profile
        /// </summary>
        /// <param name="functionName">Name of the function that is profiled (this will be seen in chromium tracing)</param>
        /// <returns>ProfileBase</returns>
        public ProfileBase? CreateProfile(string functionName)
        {
            if (!IsEnabled)
                return null;

            var profile = new ChromiumProfile(this, functionName)
            {
                StartOnTick = Stopwatch.ElapsedTicks
            };
            return profile;
        }


        /// <summary>
        /// Write profile body to file
        /// </summary>
        /// <param name="profile">Profile to be written</param>
        public void WriteProfile(ProfileBase profile)
        {
            if (!IsEnabled)
                return;

            // Duration in microseconds
            var duration = profile.Stopwatch.ElapsedTicks / (TimeSpan.TicksPerMillisecond / 10);
            
            var data = "{" + 
            "\"cat\":\"function\"," + 
            "\"dur\":" + duration + ","+
            "\"name\":\""+ profile.Name + "\","+
            "\"ph\":\"X\","+
            "\"pid\":"+ Process.GetCurrentProcess().Id +","+ 
            "\"tid\":"+ Thread.CurrentThread.ManagedThreadId + "," +
            "\"ts\":" + (profile.StartOnTick) / (TimeSpan.TicksPerMillisecond / 10)+
            "},";
            
            WriteToStream(data);
        }

        #endregion
    }
}