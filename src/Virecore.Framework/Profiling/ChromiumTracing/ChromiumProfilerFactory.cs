using System.Collections.Concurrent;
using Microsoft.Extensions.Configuration;

namespace Virecore.Profiling.ChromiumTracing
{
    public class ChromiumProfilerFactory : IProfilerFactory
    {
        #region Protected Members

        /// <summary>
        /// Configuration to use for profiler
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Path to log to
        /// </summary>
        private readonly string _filePath;

        /// <summary>
        /// List of profilers
        /// </summary>
        private readonly ConcurrentDictionary<string, ChromiumProfiler> _profilers = new ConcurrentDictionary<string, ChromiumProfiler>();
        #endregion

        #region Constructors
        
        public ChromiumProfilerFactory(string filePath, IConfiguration configuration)
        {
            _filePath = filePath;
            _configuration = configuration;
        }

        #endregion

        #region Public members

        /// <summary>
        /// Create a new Profiler service
        /// </summary>
        /// <param name="profilerName">Name of the Profiler</param>
        /// <returns>Profiler service with created name</returns>
        public IProfiler CreateProfilerService(string profilerName)
        {
            return _profilers.GetOrAdd(profilerName, name => new ChromiumProfiler(_filePath, _configuration));
        }
        
        /// <summary>
        /// Clear when done
        /// </summary>
        public void Dispose()
        {
            _profilers.Clear();
        }

        #endregion
    }
}