namespace Virecore.Profiling.ChromiumTracing
{
    public class ChromiumProfile : ProfileBase
    {
        #region Private members
        
        // Parent profiler
        private readonly IProfiler _profiler;
        
        #endregion

        #region Constructors
        
        public ChromiumProfile(IProfiler profiler,string functionName)
        {
            _profiler = profiler;
            Name = functionName;
            Start();
        }

        #endregion

        #region Public methods
        /// <summary>
        /// Stops the profile and uses parent profiler to write it to file
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            Stop();
            _profiler.WriteProfile(this);
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Stop the current profile
        /// </summary>
        private void Stop()
        {
            Stopwatch.Stop();
        }

        /// <summary>
        /// Start the current profile
        /// </summary>
        private void Start()
        {
            Stopwatch.Start();
        }

        #endregion
    }
}