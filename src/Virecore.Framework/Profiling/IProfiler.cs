namespace Virecore.Profiling
{
    public interface IProfiler
    {
        bool IsEnabled { get; }
        bool IsRunning { get; set; }
        void StartProfiling(string? sessionName = null);
        void StopProfiling();
        ProfileBase? CreateProfile(string functionName);
        void WriteProfile(ProfileBase profile);
    }
}