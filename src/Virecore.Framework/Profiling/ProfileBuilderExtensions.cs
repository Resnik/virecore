using Microsoft.Extensions.DependencyInjection;

namespace Virecore.Profiling
{
    public static class ProfileBuilderExtensions
    {
        /// <summary>
        /// Add the Profiler Factory to service collection
        /// </summary>
        /// <param name="builder">Profiler Builder</param>
        /// <param name="factory">Profiler Factory</param>
        /// <returns></returns>
        public static IProfilerBuilder AddProfilerFactory(this IProfilerBuilder builder, IProfilerFactory factory)
        {
            builder.Services.AddSingleton(factory);
            return builder;
        }
    }
}