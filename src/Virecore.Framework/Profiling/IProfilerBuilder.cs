using Microsoft.Extensions.DependencyInjection;

namespace Virecore.Profiling
{
    public interface IProfilerBuilder
    {
        IServiceCollection Services { get; }
    }
}