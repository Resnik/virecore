using System;
using System.Diagnostics;

namespace Virecore.Profiling
{
    public abstract class ProfileBase : IDisposable
    {
        #region Public members
        
        private static int ProfileCount { get; set; }
        public string Name { get; protected set; } = "NONAME";
        public long StartOnTick { get; set; }
        public Stopwatch Stopwatch { get; } = new Stopwatch();

        #endregion

        #region Protected members

        protected virtual void Dispose(bool disposing) { }

        #endregion

        #region Constructors

        protected ProfileBase()
        {
            ProfileCount++;
        }

        #endregion

        #region Public methods

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}