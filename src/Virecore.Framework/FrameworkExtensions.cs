﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Virecore.Profiling;

namespace Virecore
{
    /// <summary>
    /// Extension methods for framework
    /// </summary>
    public static class FrameworkExtensions
    {
        /// <summary>
        /// Default logger for non-generic ILogger with name "Virecore"
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        internal static IServiceCollection AddDefaultLogger(this IServiceCollection services)
        {
            // Add default logger. Add it every time it's requested.
            services.AddTransient(provider => provider.GetService<ILoggerFactory>().CreateLogger("Virecore.Logger"));
            return services;
        }

        /// <summary>
        /// Add Profiling services
        /// </summary>
        /// <param name="services"></param>
        /// <param name="profilerBuilder"></param>
        /// <returns></returns>
        public static IServiceCollection AddProfiling(this IServiceCollection services, Action<IProfilerBuilder> profilerBuilder)
        {
            services.AddOptions();
            profilerBuilder(new ProfilerBuilder(services));
            return services;
        }

        /// <summary>
        /// Add Default profiler named "Virecore.Profiler"
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddDefaultProfiler(this IServiceCollection services)
        {
            services.AddSingleton(provider => provider.GetService<IProfilerFactory>().CreateProfilerService("Virecore.Profiler"));
            return services;
        }
    }
}
