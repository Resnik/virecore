﻿using Microsoft.Extensions.Logging;

namespace Virecore.Logging.File
{
    /// <summary>
    /// The configuration for a <see cref="FileLogger"/>
    /// </summary>
    public class FileLoggerConfiguration
    {
        #region Public Properties

        /// <summary>
        /// Log level
        /// </summary>
        public LogLevel LogLevel {get;set;} = LogLevel.Trace;

        /// <summary>
        /// If time should be a part of the message
        /// </summary>
        public bool LogTime { get; set; } = true;

        #endregion
    }
}
