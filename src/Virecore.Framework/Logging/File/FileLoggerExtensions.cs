﻿using Microsoft.Extensions.Logging;

namespace Virecore.Logging.File
{
    /// <summary>
    /// Extensions for <see cref="FileLogger"/>
    /// </summary>
    public static class FileLoggerExtensions
    {
        /// <summary>
        /// Add file logger for specific path
        /// </summary>
        /// <param name="builder">Log builder to add to</param>
        /// <param name="path">Path to write to</param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static ILoggingBuilder AddLogFile(this ILoggingBuilder builder, string path, FileLoggerConfiguration? configuration = null)
        {
            if (configuration == null)
                configuration = new FileLoggerConfiguration();

            // Add file log provider to builder
            builder.AddProvider(new FileLoggerProvider(path, configuration));
            return builder;
        }
    }
}
