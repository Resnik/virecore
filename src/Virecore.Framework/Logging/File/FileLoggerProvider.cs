﻿using System.Collections.Concurrent;
using Microsoft.Extensions.Logging;

namespace Virecore.Logging.File
{
    /// <summary>
    /// Provides ability to log to a file
    /// </summary>
    public class FileLoggerProvider : ILoggerProvider
    {
        #region Protected Members

        /// <summary>
        /// Configuration to use for logger
        /// </summary>
        private readonly FileLoggerConfiguration _configuration;

        /// <summary>
        /// Path to log to
        /// </summary>
        private readonly string _filePath;

        /// <summary>
        /// List of loggers
        /// </summary>
        private readonly ConcurrentDictionary<string, FileLogger> _loggers = new ConcurrentDictionary<string, FileLogger>();
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="path">Path to write to</param>
        /// <param name="configuration">configuration to pass</param>
        public FileLoggerProvider(string path, FileLoggerConfiguration configuration)
        {
            _configuration = configuration;
            _filePath = path;
        }
        #endregion 

        /// <summary>
        /// Create FileLogger based on category name
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public ILogger CreateLogger(string categoryName)
        {
            return _loggers.GetOrAdd(categoryName, name => new FileLogger(_filePath, _configuration));
        }

        /// <summary>
        /// Clear when done
        /// </summary>
        public void Dispose()
        {
            _loggers.Clear();
        }
    }
}
