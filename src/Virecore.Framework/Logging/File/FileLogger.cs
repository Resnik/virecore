﻿using System;
using System.Collections.Concurrent;
using System.IO;
using Microsoft.Extensions.Logging;

namespace Virecore.Logging.File
{
    /// <summary>
    /// Logger that writes messages to file
    /// </summary>
    public class FileLogger : ILogger
    {
        #region Static Properties
        /// <summary>
        /// List of file lock based on path
        /// </summary>
        private static readonly ConcurrentDictionary<string, object> Locks = new ConcurrentDictionary<string, object>();
        
        #endregion

        #region Protected Members

        //private string _categoryName;
        private readonly string _filePath;
        private readonly FileLoggerConfiguration _configuration;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="filePath">Path to write to</param>
        /// <param name="configuration">Configuration to use</param>
        public FileLogger(string filePath, FileLoggerConfiguration configuration)
        {
            filePath = Path.GetFullPath(filePath);

            //_categoryName = categoryName;
            _filePath = filePath;
            _configuration = configuration;
        }

        #endregion

        /// <summary>
        /// Does not need to be scoped
        /// </summary>
        /// <typeparam name="TState"></typeparam>
        /// <param name="state"></param>
        /// <returns></returns>
        public IDisposable? BeginScope<TState>(TState state)
        {
            return null;
        }

        /// <summary>
        /// If log level is the same or greater than the configuration
        /// </summary>
        /// <param name="logLevel">log level to check against</param>
        /// <returns></returns>
        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel >= _configuration.LogLevel;
        }

        /// <summary>
        /// Logs the message
        /// </summary>
        /// <typeparam name="TState">Type of details</typeparam>
        /// <param name="logLevel">Log level</param>
        /// <param name="eventId">Id</param>
        /// <param name="state">Details of the message</param>
        /// <param name="exception">Any exception</param>
        /// <param name="formatter">Formatter for convertiog the state and exception to message</param>
        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (!IsEnabled(logLevel))
                return;

            var currentDateTime = DateTimeOffset.Now.ToString("yyyy-MM-dd hh:mm:ss");

            var timeLogString = _configuration.LogTime ? $"[{currentDateTime}] " : "";

            var stateMessage = formatter(state, exception);
            var message = $"{timeLogString}{stateMessage}{System.Environment.NewLine}";

            // Normalize path
            var normalizedPath = _filePath.ToUpper();
            // Get the file lock on abs path
            var fileLock = Locks.GetOrAdd(normalizedPath, path => new object());

            lock (fileLock)
            { 
                var dir = Path.GetDirectoryName(_filePath);
                if(string.IsNullOrEmpty(dir))
                    return;
                
                Directory.CreateDirectory(dir);
                System.IO.File.AppendAllText(_filePath, message);
            }

        }
    }
}
