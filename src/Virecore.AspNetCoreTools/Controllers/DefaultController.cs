﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Virecore.EntityFrameworkTools.Models;
using Virecore.EntityFrameworkTools.Repositories;

namespace Virecore.AspNetCoreTools.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class DefaultController<TEntity, TId, TDbContext> : ControllerBase 
        where TDbContext : DbContext 
        where TEntity : DatabaseModelBase<TId>
    {
        private readonly EntityRepository<TDbContext, TId> _entityRepository;
        private readonly ILogger _logger;
        public DefaultController(EntityRepository<TDbContext, TId> entityRepository,
            ILoggerFactory loggerFactory)
        {
            _entityRepository = entityRepository;
            _logger = loggerFactory.CreateLogger("ControllerLogger");
        }

        [HttpGet]
        public virtual async Task<ActionResult> Get()
        {
            var entities = await _entityRepository.GetEntities<TEntity>().ToArrayAsync();
            if (entities.Length > 0)
                return Ok(entities);

            return NotFound();
        }

        [HttpGet("{id}")]
        public virtual async Task<ActionResult<TEntity>> Get(TId id)
        {
            var entity = await _entityRepository.GetEntity<TEntity>(id).FirstOrDefaultAsync();
            if (entity != null)
                return Ok(entity);

            return NotFound();
        }

        [HttpPost]
        public virtual async Task<ActionResult<TEntity>> Post([FromBody] TEntity entity)
        {
            var success = await _entityRepository.CreateEntity(entity);
            if (!success) return BadRequest();
            var url = string.Format("{0}://{1}{2}/{3}",
                HttpContext.Request.IsHttps ? "https" : "http",
                HttpContext.Request.Host,
                HttpContext.Request.Path,
                ((dynamic)entity).Id.ToString());
            return Created(url, entity);

        }

        [HttpPatch("{id}")]
        public virtual async Task<ActionResult<TEntity>> Patch(TId id, [FromBody] TEntity entity)
        {
            var success = await _entityRepository.UpdateEntity(entity);
            if (!success) return BadRequest();
            entity = await _entityRepository.GetEntity<TEntity>(id).FirstOrDefaultAsync();
            return Ok(entity);
        }

        [HttpDelete("{id}")]
        public virtual async Task<ActionResult> Delete(TId id, bool markForDeletion)
        {
            var success = await _entityRepository.DeleteEntity<TEntity>(id, markForDeletion);
            if (success)
                return Ok();
            return BadRequest();
        }
    }
}