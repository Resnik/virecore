# Virecore Framework
Virecore is a .NET Core framework that makes use of Dependency Injection, Configuration, Logging, Environment and Profiling

## Table Of Content
- [Appsettings](#appsettings)
- [Framework Startup](#framework-startup)
- [Environment](#environment)
- [Configuration](#configuration)
- [Services (Dependency injection)](#services-(dependency-injection))
- [Logging](#logging)
- [Profiling](#profiling)
- [Other Authors](#other-authors)


## Appsettings

This is an example of `appsettings.json` file. This file is **not optional**.
```json
{
  "Virecore": {
    "Logging": {
      "IncludesScopes": false,
      "LogLevel": {
        "Default": "Warning"
      },
      "LogFilePath": "Log.txt"
    },
    "Profiling": {
      "Enabled": true,
      "ProfileFilePath": "Virecore_profile.json"
    }
  }
}
```
You can also add `appsettings.Development.json`. This enables you to separate your production and development settings.  
Framework will determine what file to use based on [Environment](#environment).

## Framework Startup

`Framework` class is `Static` so you only need to use `Framework.Startup()` at the beginning for your program to use it everywhere.

## Environment

Framework can determine if current environment is `Development` or `Production`.  
This is directly bound to current Visual Studio Configuration `DEBUG` and `RELEASE`.

### Usage:
```csharp
using Virecore;

namespace FooBarNamespace
{
    class Program
    {
        public static void Main()
        {
            // Start the framework
            Framework.Startup();

            // Check environment
            if(Framework.Environment.IsDevelopment)
            {
                //... this will run if "DEBUG" configuration is used in VS
            }

            // Print environment configuration "Development" or "Production"
            Console.WriteLine($"Running in {Framework.Environment.Configuration} configuration");
        }
    }
}
```

## Configuration

Configuration is done with standard Microsoft `IConfiguration` interface.  
By default you can use key value pairs from this sources:
- appsettings.json (**this file must exist!**)
- appsettings.Development.json (optional)
- Environment variables

You can add additional options with `ConfigurationBuilder` on `Framework.Startup(Action<IConfigurationBuilder> configure, ...)`

### Usage:
```csharp
using Virecore;
using Microsoft.Extensions.Configuration;

namespace FooBarNamespace
{
    class Program
    {
        public static void Main()
        {
            // Start the framework
            Framework.Startup(
                // add custom configuration
                configure => // configure is IConfigurationBuilder
                {
                    // add another json file
                    configure.AddJsonFile("some-other-file.json");

                    // add in memory collection. IEnumerable<KeyValuePair<string, string>>
                    configure.AddInMemoryCollection(new Dictionary<string,string>(){
                        {"FooKey","FooValue"},
                        {"BarKey","BarValue"}
                    });
                },
                // services
                null
            );
        }
    }
}
```

You can access your configuration with Framework property: `Framework.Configuration`
```csharp
var configuration = Framework.Configuration;
var someValue = configuration.GetSection("SomeSection").GetValue<string>("SomeKey");
```

## Services (Dependency injection)

Services are added with standard Microsoft `IServiceCollection` interface.  

Every service can be accessed with `Framework.GetService<YOUR_SERVICE_INTERFACE>()`

By default Framework contains these services:
- `Framework.Environment` same as `Framework.GetService<FrameworkEnvironment>()`
- `Framework.Configuration` same as `Framework.GetService<IConfiguration>()`
- `Framework.Logger` same as `Framework.GetService<ILogger>()`
- `Framework.Profiler` same as `Framework.GetService<IProfiler>()`

You can add additional services with `IServiceCollection` on `Framework.Startup(..., Action<IServiceCollection> services)`

### Usage:
```csharp
using Virecore;

namespace FooBarNamespace
{
    class Program
    {
        public static void Main()
        {
            // Start the framework
            Framework.Startup(
                // configuration
                null,
                // add custom services
                services => // services is IServiceCollection
                {
                    services.AddTransient<IFoo, Foo>();
                    services.AddSingleton<IBar, Bar>();
                }
            );
        }
    }
}
```

## Logging

Logging can be configured in `Logging` section in [Appsettings](#appsettings).  
- `IncludesScopes` - ***not working at the moment***.
- `LogFilePath` - path to log file.
- `LogLevel` - lowest level to log.  

>Log levels:
>- Trace = 0  
>- Debug = 1  
>- Information = 2  
>- Warning = 3  
>- Error = 4  
>- Critical = 5  
>- None = 6


Logger is implemented with standard Microsoft `ILogger` interface.  
On top of that you can use extension methods for additional info:
- `LogInformationSource("message")`
- `LogTraceSource("message")`
- `LogDebugSource("message")`
- `LogWarningSource("message")`
- `LogErrorSource("message")`
- `LogCriticalSource("message")`

Methods with `Source` suffix will also log `file name`, `calling method` and `line number` for easier debugging.

### usage:

```csharp
using Virecore;
using Virecore.Logging;

namespace FooBarNamespace
{
    class Program
    {
        public static void Main()
        {
            // Start the framework
            Framework.Startup();

            // Log someting with Source suffix
            Framework.Logger.LogInformationSource("This is some logged information");
        }
    }
}

```

Output:

```console
info: Virecore.Logger[0]
      This is some logged information [Program.cs > Main() > Line 33]
```


## Profiling

Profiling is currently done with chromium (or chrome) tracing.  
Framework creates the json file specified in [Appsettings](#appsettings) under `Profiling` > `ProfileFilePath`.  
Session name will be added to specified filename if provided.  

This file can then be imported in chromium or chrome:
1. Open chromium and navigate to `chrome://tracing`.
2. Load the json profile file.

### Usage:

```csharp
using Virecore;

namespace FooBarNamespace
{
    class Program
    {
        public static void Main()
        {
            // Start the framework
            Framework.Startup();

            // Tell profiler to make a new session
            Framework.Profiler.StartProfiling("SESSION NAME");
            
            // CreateProfile returns ProfileBase which is IDisposable.
            // After the 'using' scope ends, Dispose() kicks in and writes the profile to file
            using (Framework.Profiler.CreateProfile("Some friendly name"))
            {
                //... Some complicated task that needs profiling ...
            }

            // stop the current session
            Framework.Profiler.StopProfiling();
        }
    }
}

```
You could use this pattern on every method (with some little overhead)

```csharp
// you can use 'using' without defining scope.
// this 'using' will dispose at the end of current scope.

public  void FooFunction()
{ 
    using var profile = Framework.Profiler.CreateProfile("Some friendly name"));

    //some function logic that has to be profiled

} // <-- profile disposes here

```

### Example:
Example screenshot of Chromium Tracing

![Example of Chromium Tracing](./etc/Images/ChromiumTracingExample.png)

## Other Authors
Icon made by [Flat icons](https://www.flaticon.com/authors/flat-icons) from [www.flaticon.com](www.flaticon.com)